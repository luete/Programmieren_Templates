package de.dhbw.ka.prefabs.examples;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import de.dhbw.ka.prefabs.Highscores;
import de.dhbw.ka.prefabs.Highscores.Score;

public class HighscoresExample
{
	public static void main(String[] args) throws IOException
	{
		Highscores highscore = new Highscores();
		highscore.add("Frank", 0);
		highscore.add("Peter", 1);
		highscore.add("Mario", 42);

		highscore.serialize();
		highscore.deserialize();
		
		highscore.getScores().forEach(Score::print);
		
		Files.deleteIfExists(Paths.get(Highscores.PATH));
	}
}
