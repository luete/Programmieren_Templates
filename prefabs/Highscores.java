package de.dhbw.ka.prefabs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Highscores
{
	public static final String PATH = "./highscore.txt";
	
	private List<Score> scores = new ArrayList<>();
	
	public void add(String userName, int score)
	{
		scores.add(new Score(userName, score));
	}
	
	public List<Score> getScores()
	{
		Collections.sort(scores, Collections.reverseOrder());
		return scores;
	}
	
	public void serialize() throws IOException
	{
		List<String> scoreStrings = getScores().stream()
										.map(Score::toString)
										.collect(Collectors.toList());
		
		String fileContent = String.join("\r\n", scoreStrings);
		
		File file = new File(PATH);
		if (!file.exists())
		{
			if (!file.getParentFile().exists())
			{
				file.getParentFile().mkdirs();
			}

			file.createNewFile();
		}

		try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, false)))
		{
			writer.write(fileContent);
		}
	}
	
	public void deserialize() throws IOException
	{
		scores.clear();
		
		try (BufferedReader reader = new BufferedReader(new FileReader(PATH)))
		{
			reader.lines()
					.filter(line -> !line.isEmpty())
					.map(Score::fromString)
					.forEach(scores::add);
		}
		
		Collections.sort(scores, Collections.reverseOrder());
	}

	public static class Score implements Comparable<Score>
	{
		private final String userName;
		private final int score;

		private Score(String userName, int score)
		{
			this.userName = userName;
			this.score = score;
		}

		public String getUserName()
		{
			return userName;
		}

		public int getScore()
		{
			return score;
		}
		
		public void print()
		{
			System.out.println(getUserName() + " " + getScore());
		}

		@Override
		public int compareTo(Score o)
		{
			return Integer.compare(this.score, o.score);
		}
		
		public String toString()
		{
			return getUserName() + ";" + getScore();
		}
		
		public static Score fromString(String serializedString)
		{
			String[] data = serializedString.split(";");
			return new Score(data[0], Integer.parseInt(data[1]));
		}
	}
}
