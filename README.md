# Verwendungen
Sofern die Templates importiert wurden, können diese ganz einfach mithilfe von Intellisense (Content assist) verwendet werden. Tippe den Anfang eines Templatenamens ein (z.B. file_r für file_read) und bestätige mit Enter oder wähle das passende Template aus.
Alle Templates beginnen mit einem Präfix.

# Prefabs
Der Ordner Prefabs enthält vorgefertigte Klassen die in der Regel, so wie sie sind kopiert und für das Projekt genutzt werden können. Sie enthalten auch Beispieldateien.
* Highscores.java
* Player.java

# Template Auflistung
* **file_ → Datei Operationen**
	* file_read
	* file_write
	* file_delete
* **sw_ → Swing Templates**
	* sw_actionlistener
	* sw_filechooser <br />
	FileChooser mit Filter für Dateitypen und allen nötigen Überprüfungen
	* sw_methods_createButton <br />
	Erstellt eine Methode, welche das erstellen von Buttons und setzen des ActionListeners in einem ermöglicht (nur für Lambda User, vermutlich unnötig)
* **sw_layout_ →  Layouts  mit Beispieldaten** <br />
    Bilder zu den Layouts finden sich im "Images" Ordner. Alle Layouts haben standardmäßig ein padding von 10px (panel.setBorder) und Spacing von 5 (Abstand zwischen Elementen). Die Rote Linie auf den Bildern markiert die vom Template generierten Zeilen.
	* sw_layout_box
	* sw_layout_grid
* **thread_ → Parallele Operationen**
    * thread_timer <br />
    Erstellt einen Timer der in regelmäßigen Abständen die gleiche Logik ausführt und das Userinterface aktualisiert.
    * thread_timer_countdownLabel <br />
    Erstellt ein Label & Timer. Das Label wird sekündlich heruntergezählt und kann bei 0 eine Aktion ausführen.

# Importieren
![N|Solid](https://proxy.duckduckgo.com/iu/?u=http%3A%2F%2Fhowtodoinjava.com%2Fwp-content%2Fuploads%2F2014%2F05%2Feclipse-template.png&f=1)