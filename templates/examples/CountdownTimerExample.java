package de.dhbw.ka.templates.examples;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class CountdownTimerExample
{
	private static final int COUNTDOWN_START = 5;
	
	public static void main(String[] args)
	{
		JFrame wnd = new JFrame("Countdown Timer");
		wnd.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JLabel lblCountdown = new JLabel("" + COUNTDOWN_START);
		
		JPanel panel = new JPanel();
		GridLayout gridLayout = new GridLayout(0, 2, 5, 5);
		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panel.setLayout(gridLayout);

		panel.add(lblCountdown);
		
		wnd.setContentPane(panel);
		wnd.pack();
		wnd.setVisible(true);
		
		Timer timer = new Timer(true);
		timer.scheduleAtFixedRate(new TimerTask()
		{
			private AtomicInteger counter = new AtomicInteger(COUNTDOWN_START);
			
			@Override
			public void run()
			{
				int currentValue = counter.decrementAndGet();
				
				updateOperation(currentValue, lblCountdown);

				if (currentValue == 0)
				{
					finishedOperation(lblCountdown);
					this.cancel(); //Timer bzw diesen Task beenden
				}
			}
		}, 0, 1000);
	}
	
	private static void updateOperation(int currentValue, JLabel lblCountdown)
	{
		lblCountdown.setText("" + currentValue);
		if(currentValue % 2 == 0)
		{			
			lblCountdown.setForeground(Color.MAGENTA);
		}
		else
		{
			lblCountdown.setForeground(Color.BLUE);
		}
	}
	
	private static void finishedOperation(JLabel lblCountdown)
	{
		lblCountdown.setForeground(Color.RED);
		JOptionPane.showMessageDialog(null, "Zeit abgelaufen");
	}
}
