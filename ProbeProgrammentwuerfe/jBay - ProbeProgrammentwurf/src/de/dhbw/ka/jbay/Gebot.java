package de.dhbw.ka.jbay;

public class Gebot
{
	private final double _maxBetrag;
	private final Bieter _bieter;

	public Gebot(Bieter bieter, double maxBetrag)
	{
		_bieter = bieter;
		_maxBetrag = maxBetrag;
	}

	public Bieter getBieter()
	{
		return _bieter;
	}

	public double getMaxBetrag()
	{
		return _maxBetrag;
	}
}
