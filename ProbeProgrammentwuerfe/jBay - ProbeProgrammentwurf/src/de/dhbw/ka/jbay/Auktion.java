package de.dhbw.ka.jbay;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;

public class Auktion
{
	public static final double INCREMENT = 1.0;
	private final Calendar _auktionsEnde;

	private double _aktuellerPreis = 0;
	private Gebot _aktuellesGebot = null;
	private final Ware _ware;

	public Auktion(Ware ware, int dauer)
	{
		_ware = ware;
		_auktionsEnde = Calendar.getInstance();
		_auktionsEnde.setTimeInMillis(System.currentTimeMillis() + 60000 * dauer);
	}

	public Ware getWare()
	{
		return _ware;
	}

	public double getAktuellerPreis()
	{
		return _aktuellerPreis;
	}

	public Gebot getHoechstgebot()
	{
		return _aktuellesGebot;
	}

	public Calendar getAuktionsEnde()
	{
		return _auktionsEnde;
	}

	public boolean gebotAbgeben(Gebot g)
	{
		logGebot(g);
		
		if (g.getMaxBetrag() < _aktuellerPreis + INCREMENT)
		{
			return false;
		}

		if (_aktuellesGebot == null)
		{
			_aktuellerPreis = INCREMENT;
			_aktuellesGebot = g;
			return true;
		}

		if (_aktuellesGebot.getBieter() == g.getBieter())
		{
			if (g.getMaxBetrag() > _aktuellesGebot.getMaxBetrag())
			{
				_aktuellesGebot = g;
				return true;
			}

			return false;
		}

		if (g.getMaxBetrag() >= _aktuellerPreis + INCREMENT)
		{
			if (g.getMaxBetrag() <= _aktuellesGebot.getMaxBetrag())
			{
				_aktuellerPreis = Math.min(g.getMaxBetrag() + INCREMENT, _aktuellesGebot.getMaxBetrag());
				return false;
			}

			if (g.getMaxBetrag() > _aktuellesGebot.getMaxBetrag())
			{
				_aktuellerPreis = Math.min(g.getMaxBetrag(), _aktuellesGebot.getMaxBetrag() + INCREMENT);
				_aktuellesGebot = g;
				return true;
			}
		}

		return false;
	}

	private void logGebot(Gebot g)
	{
		try
		{
			String newLine = "[" + Calendar.getInstance().getTime() + "] Gebot von" + g.getBieter().getFullName()
					+ " f�r " + _ware.getTitel() + ": " + g.getMaxBetrag() + " Euro.\r\n";
			
			File file = new File("./auktionen.txt");
			if (!file.exists())
			{
				if (!file.getParentFile().exists())
				{
					file.getParentFile().mkdirs(); // Erstellt Ordner falls noch nicht existierend
				}

				file.createNewFile(); // Erstellt Datei falls noch nicht existiert.
			}

			try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, true)))
			{
				writer.write(newLine);
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
