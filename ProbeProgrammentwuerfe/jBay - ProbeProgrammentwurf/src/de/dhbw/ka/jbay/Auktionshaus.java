package de.dhbw.ka.jbay;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class Auktionshaus
{
	private List<Auktion> _auktionen = new ArrayList<>();
	private List<BieterTerminal> _terminals = new ArrayList<>();
	
	public Auktionshaus()
	{
		Timer timer = new Timer(true);
		timer.scheduleAtFixedRate(new TimerTask()
		{
			@Override
			public void run()
			{
				updateTerminals();
			}
		}, 0, 1000);
	}

	public void addAuktion(Auktion a)
	{
		_auktionen.add(a);
	}
	
	public void removeAuktion(Auktion a)
	{
		_auktionen.remove(a);
	}
	
	public List<Auktion> getAuktionen()
	{
		return _auktionen;
	}

	public void register(BieterTerminal bt)
	{
		_terminals.add(bt);
	}
	
	public void unregister(BieterTerminal bt)
	{
		_terminals.remove(bt);
	}
	
	private void updateTerminals()
	{
		_terminals.forEach(BieterTerminal::update);
	}
}
