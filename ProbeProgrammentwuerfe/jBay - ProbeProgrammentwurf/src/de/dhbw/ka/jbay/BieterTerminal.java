package de.dhbw.ka.jbay;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class BieterTerminal
{
	private final JFrame _window = new JFrame("");

	private Bieter _bieter;
	private Auktionshaus _auktionshaus;

	private JPanel _mainPanel;
	private JPanel _gridPanel;
	private JLabel _lblCurrentTime;

	public BieterTerminal(Bieter bieter, Auktionshaus ah)
	{
		_bieter = bieter;
		_auktionshaus = ah;

		_window.setTitle(bieter.getFullName() + "'s Terminal");
		_window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		_window.setResizable(false);

		initLayout();
		initComponents();
	}

	public void show()
	{
		update();
		_window.setVisible(true);
		_window.pack();
	}

	private void initComponents()
	{
		_lblCurrentTime = new JLabel("");
		_mainPanel.add(_lblCurrentTime);
		_mainPanel.add(Box.createRigidArea(new Dimension(5, 5)));
		_mainPanel.add(_gridPanel);
	}

	private void initLayout()
	{
		_mainPanel = new JPanel();
		_gridPanel = new JPanel();

		_mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		_mainPanel.setLayout(new BoxLayout(_mainPanel, BoxLayout.PAGE_AXIS));

		GridLayout gridLayout = new GridLayout(0, 5, 5, 5);
		_gridPanel.setLayout(gridLayout);

		_window.add(_mainPanel);
	}

	public void update()
	{
		_lblCurrentTime.setText(Calendar.getInstance().getTime().toString());
		_gridPanel.removeAll();

		initTableHeader();
		for (Auktion a : _auktionshaus.getAuktionen())
		{
			_gridPanel.add(new JLabel(a.getWare().getTitel()));
			_gridPanel.add(new JLabel("" + a.getAktuellerPreis()));

			String bieterName = a.getHoechstgebot() == null ? "---" : a.getHoechstgebot().getBieter().getFullName();
			_gridPanel.add(new JLabel(bieterName));
			_gridPanel.add(new JLabel(a.getAuktionsEnde().getTime().toString()));

			JButton btnGebot = new JButton("Gebot");
			_gridPanel.add(btnGebot);

			btnGebot.addActionListener(createGebotsListener(a));
		}

		_window.pack();
	}
	
	private void initTableHeader()
	{
		_gridPanel.add(new JLabel("Ware"));
		_gridPanel.add(new JLabel("Preis"));
		_gridPanel.add(new JLabel("Höchstbietender"));
		_gridPanel.add(new JLabel("Auktionsende"));
		_gridPanel.add(new JLabel("Gebot abgeben"));
	}

	private ActionListener createGebotsListener(final Auktion a)
	{
		return new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				double initialValue = a.getAktuellerPreis() + Auktion.INCREMENT;
				String neuesGebot = JOptionPane.showInputDialog(
						"Bitte neues Gebot eingeben für '" + a.getWare().getTitel() + "'.\nMindestens XY Euro",
						initialValue);
				double gebot = 0;

				try
				{
					gebot = Double.parseDouble(neuesGebot);
				}
				catch (Exception ex)
				{
					JOptionPane.showMessageDialog(null, "Ungültige Eingabe");
					return;
				}

				if (a.getAuktionsEnde().before(Calendar.getInstance()))
				{
					JOptionPane.showMessageDialog(null, "Die Auktion ist leider schon vorbei...");
					return;
				}

				if (a.gebotAbgeben(new Gebot(_bieter, gebot)))
				{
					JOptionPane.showMessageDialog(null, "Sie sind Höchstbietender");
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Sie sind Höchstbietender");
				}
			}
		};
	}
}
