package de.dhbw.ka.jbay;

public class Ware
{
	private final String _titel;
	private final String _beschreibung;

	public Ware(String titel, String beschreibung)
	{
		_titel = titel;
		_beschreibung = beschreibung;
	}

	public String getTitel()
	{
		return _titel;
	}

	public String getBeschreibung()
	{
		return _beschreibung;
	}
}
