package de.dhbw.ka.jbay;

public class Bieter
{
	private final String vorname;
	private final String nachname;

	public Bieter(String vorname, String nachname)
	{
		this.vorname = vorname;
		this.nachname = nachname;
	}
	
	public String getFullName()
	{
		return vorname + " " + nachname;
	}
}
